Позволяет Far использовать Cygwin bash как интерпретатор команд.
Требует Far 3.0 build 4722 или более новый.
Может быть адаптирован к другому bash.

Установка
=========

1. Скопируйте `bashcmd.exe` и `bashcmd.sh` в одну директорию на ваше усмотрение, например `C:\cygwin\usr\local\bin`.
Если хотите использовать 64-битную версию, используйте `bashcmd64.exe` (переименуйте в `bashcmd.exe`).

2. Создайте `/etc/bash.bashcmdrc` и наполните на ваше усмотрение (см. описание ниже).

3. Скопируйте `Bash.lua` в директорию макросов Фара, например `C:\Users\root\AppData\Roaming\Far Manager\Profile\Macros\scripts`.
При необходимости, поменяйте значения констант в начале файла.

4. Выполните `far:config`, и установите следующие конфигурационные опции:
Cmdline.PromptFormat $#25$p %FAR_Prompt%
Cmdline.UsePromptFormat true
System.Executor.ExcludeCmds %FAR_ExcludeCmds%
System.Executor.Comspec %FAR_Comspec%
System.Executor.ComspecArguments %FAR_ComspecArguments%
System.Executor.ComspecCondition %FAR_ComspecCondition%

Описание
========

`bashcmd.exe` - написанный мною на С++ wrapper (я использовал VS2015). Принимает командную строку, если команда - `start`, либо файл с расширением
bat|cmd|vbs|vbe|js|jse|wsf|wsh|msc, то запускает командную строку через ComSpec, иначе запускает её через `bashcmd.sh` с правильно оформленными кавычками.

`bashcmd.sh` - малюсенький bash-скрипт, который подключает `/etc/bash.bashcmdrc` и выполняет командную строку.

`/etc/bash.bashcmdrc` - пользовательский файл инициализации для функций, алиасов, переменных среды, опций шелла и т.д. Может быть пустым.
Так как bash запускается как не-интерактивный shell, то перед загрузкой алиасов эту опцию необходимо включить: `shopt -s expand_aliases`.

Если установить переменную среды `BASHCMD_DEBUG` в любое не-пустое значение, то `bashcmd.exe` и `bashcmd.sh` будут выводить отладочную информацию.
Это, в частности, позволяет легко различить, выполняется ли конкретная команда через bashcmd, или же напрямую Фаром.
Если установить `BASHCMD_DEBUG` в значение `pause`, `bashcmd.exe` будет просить нажать Ввод перед выходом.
