local run_bash = "bash"
--local run_bash_conemu = "conemu-cyg-64 bash"
local run_bash_conemu = "bash"
local run_bash_new = "run C:\\cygwin\\Cygwin.bat"

local bashcmd_dir = "C:\\cygwin\\usr\\local\\bin" 

local default_comspec = "bash"
--local default_comspec = "cmd"

local env_comspec = "FAR_Comspec"
local comspec_cmd = "C:\\Windows\\system32\\cmd.exe"
local comspec_bash = bashcmd_dir .. "\\bashcmd.exe"

local env_comspecArguments = "FAR_ComspecArguments"
local comspecArguments_cmd = '/S /C "{0}"'
local comspecArguments_bash = '/S /C "{0}"'

local env_comspecCondition = "FAR_ComspecCondition"
local comspecCondition_cmd = ""
-- Use comspec (bash) if command line contains:
--	 - `\` character in the arguments part of the command line
--	 - `$` not within single quotes
--	 - A single character from the list `[({;<>|&` not within single or double quotes
local comspecCondition_bash = "^(?:(?:\\\"[^\\\"]*\\\"|[^\\s\"]+)\\s+.*\\\\|(?:[^']|'[^']*')*?\\$|(?:[^\\\"']|\\\"[^\\\"]*\\\"|'[^']*')*?[[({;<>|&])"

local env_excludeCmds = "FAR_ExcludeCmds"
local excludeCmds_cmd = "ASSOC;BREAK;CALL;COLOR;COPY;DATE;DEL;DIR;ECHO;ENDLOCAL;ERASE;ERRORLEVEL;EXIST;EXIT;FOR;FTYPE;GOTO;IF;LABEL;MD;MKDIR;MOVE;NOT;PATH;PAUSE;POPD;PROMPT;PUSHD;RD;REN;RENAME;RMDIR;SET;SETLOCAL;SHIFT;START;TIME;TITLE;TYPE;VER;VERIFY;VOL"
local excludeCmds_bash = "(;((;.;:;[;[[;bind;builtin;case;command;coproc;declare;echo;eval;exec;exit;export;false;for;function;help;if;kill;let;mapfile;printf;read;readarray;readonly;select;shopt;source;test;time;trap;true;type;ulimit;umask;until;while;{"

local env_farPrompt = "FAR_Prompt"
local farPrompt_cmd = ">"
local farPrompt_bash = "#"

local ConEmu = "4b675d80-1d4a-4ea9-8436-fdc23f2fc14b"

local function ToggleComSpec(quiet)
	if win.GetEnv(env_comspec) ~= comspec_bash then
		win.SetEnv(env_comspec, comspec_bash)
		win.SetEnv(env_comspecArguments, comspecArguments_bash)
		win.SetEnv(env_comspecCondition, comspecCondition_bash)
		win.SetEnv(env_excludeCmds, excludeCmds_bash)
		if not quiet then
			win.SetEnv(env_farPrompt, farPrompt_bash)
		end
	else
		win.SetEnv(env_comspec, comspec_cmd)
		win.SetEnv(env_comspecArguments, comspecArguments_cmd)
		win.SetEnv(env_comspecCondition, comspecCondition_cmd)
		win.SetEnv(env_excludeCmds, excludeCmds_cmd)
		if not quiet then
			win.SetEnv(env_farPrompt, farPrompt_cmd)
		end
	end
	if not quiet then
		panel.SetCmdLine(nil, panel.GetCmdLine())
	end
end

local function InitComSpec()
	win.SetEnv("BASHCMD", bashcmd_dir .. "\\bashcmd.sh")
	if default_comspec == "bash" then
		ToggleComSpec()
	elseif default_comspec == "cmd" then
		ToggleComSpec(true)
		ToggleComSpec()
	else
		error("Invalid `default_comspec`: " .. default_comspec .. ' (valid values: bash, cmd).')
	end
end

local function ExecAltComSpec(keys)
	ToggleComSpec(true)
	if panel.GetCmdLine() == "" then
		Keys("CtrlEnter")
	end
	Keys(keys)
	ToggleComSpec(true)
end

local function RunCommand(command, keys)
	local cmdLine = panel.GetCmdLine()
	Keys("Esc")
	print(command)
	Keys(keys)
	panel.SetCmdLine(nil, cmdLine)
end

Macro {
	area="Shell"; key="RCtrlAltQ"; flags="RunAfterFARStart"; description="ComSpec: toggle between bash and cmd";
	action = InitComSpec;
}

Macro {
	area="Shell"; key="RCtrlEnter"; description="ComSpec: execute using alt shell";
	action = function()
		ExecAltComSpec("Enter")
	end;
}

Macro {
	area="Shell"; key="RCtrlShiftEnter"; description="ComSpec: execute using alt shell (new console)";
	action = function()
		ExecAltComSpec("ShiftEnter")
	end;
}

Macro {
	area="Shell"; key="RCtrlQ"; description="Bash shell";
	action = function()
		if Plugin.Call(ConEmu, "IsConEmu()") then
			RunCommand(run_bash_conemu, "Enter")
		else
			RunCommand(run_bash, "Enter")
		end
	end;
}

Macro {
	area="Shell"; key="RCtrlShiftQ"; description="Bash shell (new console)";
	action = function()
		RunCommand(run_bash_new, "Enter")
	end;
}
