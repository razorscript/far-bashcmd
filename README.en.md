Allows Far to use Cygwin bash as the command interpreter.
Requires Far 3.0 build 4722 or newer.
May be adapted to a different bash.

Installation
============

1. Copy `bashcmd.exe` and `bashcmd.sh` to a common directory of your choosing, e.g. `C:\cygwin\usr\local\bin`.
If you wish to use the 64-bit build, use `bashcmd64.exe` (rename to `bashcmd.exe`).

2. Create `/etc/bash.bashcmdrc` (see description below).

3. Copy `Bash.lua` to the Far macros directory, e.g. `C:\Users\root\AppData\Roaming\Far Manager\Profile\Macros\scripts`.
If necessary, edit the constants at the beginning of the file.

4. Execute `far:config`, and set the following configuration options:
Cmdline.PromptFormat $#25$p %FAR_Prompt%
Cmdline.UsePromptFormat true
System.Executor.ExcludeCmds %FAR_ExcludeCmds%
System.Executor.Comspec %FAR_Comspec%
System.Executor.ComspecArguments %FAR_ComspecArguments%
System.Executor.ComspecCondition %FAR_ComspecCondition%

Description
===========

`bashcmd.exe` - a С++ wrapper written by me (I used VS2015). Reads a comand line, if the command is `start`, or a file whose extension is
bat|cmd|vbs|vbe|js|jse|wsf|wsh|msc, runs the command line using ComSpec, runs it with `bashcmd.sh` with appropriately escaped quotes.

`bashcmd.sh` - tiny bash-script, sources `/etc/bash.bashcmdrc` and evaluates the command line.

`/etc/bash.bashcmdrc` - user initialization file for functions, aliases, environment variables, shell options etc. May be empty.
Because bash is executed as a non-interactive shell, before loading aliases this option should be enabled: `shopt -s expand_aliases`.

If you set the `BASHCMD_DEBUG` environment variable to any non-empty value, `bashcmd.exe` and `bashcmd.sh` will output debug information.
This, among other things, allows to quickly see if a particular command is being run using bashcmd or directly by Far.
If `BASHCMD_DEBUG` is set to `pause`, `bashcmd.exe` will prompt to press Enter before exiting.
