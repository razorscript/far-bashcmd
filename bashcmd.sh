#!/bin/bash
#
# A wrapper script called by `bashcmd.exe` (which can be used as ComSpec to execute commands using bash).
# With Far 3.0.4645 or later, make sure the far:config option `System.Executor.ComspecArguments` is set to `/S /C "{0}"`
# Set the ComSpec to point to `bashcmd.exe`, e.g. `set ComSpec=c:\cygwin\usr\local\bin\bashcmd.exe`
# @see https://en.wikipedia.org/wiki/COMSPEC

. /etc/bash.bashcmdrc
[[ $BASHCMD_DEBUG ]] && printf 'bashcmd.sh: cmd=`%s`\n' "$1" || true
eval "$1"
