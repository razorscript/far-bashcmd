// bashcmd.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#ifdef _WIN64
	#define BASHCMD_DEFAULT L"C:/cygwin64/usr/local/bin/bashcmd.sh"
#else
	#define BASHCMD_DEFAULT L"C:/cygwin/usr/local/bin/bashcmd.sh"
#endif

bool shouldPause;

void pause() {
	cerr << L"bashcmd.exe: Press Enter to continue... ";
	cin.ignore();
}

void die(string msg, int exitCode = 1) {
	cerr << L"bashcmd.exe: Error: " << msg << std::endl;
	if (shouldPause)
		pause();
	exit(exitCode);
}

void dieLastError(string msg, int exitCode = 1) {
	cerr << L"bashcmd.exe: Error " << GetLastError() << L": " << msg << std::endl;
	if (shouldPause)
		pause();
	exit(exitCode);
}

string replaceAll(string str, const string &from, const string &to) {
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // Handles case where 'to' is a substring of 'from'.
	}
	return str;
}

string quoteForBash(string cmd) {
	return replaceAll(replaceAll(cmd, L"\"", L"\"\""), L"\\", L"\\\\");
}

bool getEnvVar(const wchar_t *name, string &strBuffer) {
	WCHAR Buffer[MAX_PATH];
	// GetEnvironmentVariable doesn't change error code on success
	SetLastError(ERROR_SUCCESS);
	DWORD Size = GetEnvironmentVariable(name, Buffer, static_cast<DWORD>(std::size(Buffer)));
	const auto LastError = GetLastError();

	if (Size) {
		if (Size < std::size(Buffer)) {
			strBuffer.assign(Buffer, Size);
		}
		else {
			std::vector<wchar_t> vBuffer(Size);
			Size = GetEnvironmentVariable(name, vBuffer.data(), Size);
			strBuffer.assign(vBuffer.data(), Size);
		}
	}

	return LastError != ERROR_ENVVAR_NOT_FOUND;
}

int main() {
	string envVar, comSpec;
	getEnvVar(L"ComSpec", comSpec);
	bool isDebug = getEnvVar(L"BASHCMD_DEBUG", envVar) && (envVar != L"");
	shouldPause = isDebug && (envVar == L"pause");
	string bashCMD = (getEnvVar(L"BASHCMD", envVar) && (envVar != L"")) ? envVar : BASHCMD_DEFAULT;

	string cmdLine = GetCommandLine();
	if (isDebug)
		cerr << L"bashcmd.exe: cmdLine=`" << cmdLine << L"`" << std::endl;

	smatch rxMatch;

	regex cmdLineRX(L"(?:\"[^\"]*\"|[^\\s\"]+)\\s+[^\"]*\"((?:.|\\r|\\n)*)\"");
	if (std::regex_match(cmdLine, rxMatch, cmdLineRX))
		cmdLine = rxMatch[1];
	else
		die(L"Failed to extract command - is it enclosed in double quotes?\ncmdLine=`" + cmdLine + L"`");

	if (isDebug)
		cerr << L"bashcmd.exe: cmd=`" << cmdLine << L"`" << std::endl;

	bool useComSpec = false;
	regex fileRX(L"(\"([^\"]+)\"|([^\\s]+))([\\s]+(?:.|\\r|\\n)*)?");
	if (std::regex_match(cmdLine, rxMatch, fileRX)) {
		string file = rxMatch[2].length() ? rxMatch[2] : rxMatch[3];
		regex batchRX(L".*\\.(bat|cmd|vbs|vbe|js|jse|wsf|wsh|msc)|start", icase);
		if (useComSpec = std::regex_match(file, batchRX))
			cmdLine = replaceAll(rxMatch[1], L"/", L"\\") + rxMatch[4].str();
	}

	SHELLEXECUTEINFO seInfo = { sizeof(seInfo) };
	seInfo.nShow = SW_SHOWNORMAL;
	seInfo.fMask = SEE_MASK_NOASYNC | SEE_MASK_NOCLOSEPROCESS | SEE_MASK_NO_CONSOLE;
	seInfo.lpFile = (useComSpec ? comSpec : bashCMD).data();
	cmdLine = useComSpec ? (L"/S /C \"" + cmdLine + L"\"") : (L"\"" + quoteForBash(cmdLine) + L"\"");
	seInfo.lpParameters = cmdLine.data();
	if (isDebug) {
		cerr << L"bashcmd.exe: file=`" << seInfo.lpFile << L"`" << std::endl;
		cerr << L"bashcmd.exe: args=`" << seInfo.lpParameters << L"`" << std::endl;
	}

	signal(SIGINT, SIG_IGN);
	signal(SIGBREAK, SIG_IGN);
	
	bool result = ShellExecuteEx(&seInfo) != FALSE;
	if (!result || (seInfo.hProcess == nullptr) || (seInfo.hProcess == INVALID_HANDLE_VALUE))
		dieLastError(L"Failed to execute command.");

	DWORD exitCode;
	WaitForSingleObject(seInfo.hProcess, INFINITE);
	GetExitCodeProcess(seInfo.hProcess, &exitCode);
	CloseHandle(seInfo.hProcess);
	if (isDebug)
		cerr << L"bashcmd.exe: exitCode=" << exitCode << std::endl;

	if (shouldPause)
		pause();
	return exitCode;
}
