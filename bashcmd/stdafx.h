// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "targetver.h"

#include <windows.h>
#include <shellapi.h>
#include <signal.h>

#include <iostream>
#define cin std::wcin
#define cout std::wcout
#define cerr std::wcerr

#include <string>
using string = std::wstring;
using namespace std::string_literals;

#include <regex>
using regex = std::wregex;
using cmatch = std::wcmatch;
using smatch = std::wsmatch;
using namespace std::regex_constants;

#include <vector>
